const config: any = {
    DB: 'tutto',
    HOST: 'localhost',

    // DB Table
    TABLE_LOG: 'log',
    TABLE_SPECIAL_TERMS: 'special_terms',
    TABLE_COMPANY_ATTRIBUTE: 'company_attribute',
    TABLE_ITEMS: 'items',
    TABLE_SUB_ATTRIBUTES: 'sub_attributes',
    TABLE_COVERAGE: 'coverage',
    TABLE_COVERAGE_COMPANY: 'coverage_company',
    TABLE_PACKAGES: 'packages'
};

config.DB_TABLE = [
    {name: config.TABLE_LOG, indexs: ['date']},
    {name: config.TABLE_SUB_ATTRIBUTES},
    {name: config.TABLE_COVERAGE},
    {name: config.TABLE_COVERAGE_COMPANY},
    {name: config.TABLE_PACKAGES},
    {name: config.TABLE_COMPANY_ATTRIBUTE, indexs: ['company']},
    {name: config.TABLE_ITEMS, indexs: ['name', 'matchingAttribute', 'condition']}
];

export const Config = config;
