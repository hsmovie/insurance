import { InputCoverage } from './coverage.interface';

export interface SubAttributes {
    who: string[];
    when1: string[];
    when2: string[];
    what: string[];
}

export interface Item {
    matchingAttribute: MatchingAttribute;
    condition: Condition;
    company?: string;
    title?: string;
}

export interface ItemGroupsByAttribute {
    [key: string]: ItemBuyItemGroups;
}

export interface ItemBuyItemGroups {
    groupSize: number;
    items: Item[];
    attributes: string[];
    score: { overCount: number, sameCount: number, needCount: number };
    autoAddItems?: Item[];
    selectAttribute?: string[];
}

export interface MatchingAttribute {
    subAttributeGroups: string[];
    // 대인2 속성
    legalSupport?: boolean; // 형사상 책임 포괄
    // 대물 속성
    foreignCarAccident?: boolean; // 외산차 사고

    // 무보험 속성
    driveOtherCar?: boolean; // 다른 자동차 운전 확대.
    driverRangeOtherCar?: boolean; // 다른 차 보상에 대한 운전자범위 확대.
    damageOtherCar?: boolean; // 다른 차 차량손해.
    substitutiveDriver?: boolean; // 대리운전 운전담보.
    damageByOtherCar?: boolean; // 다른 차에 의한 사고에 대해 보상확대

    // 자기차량손해
    expandRangeSelfCar?: boolean; // 차량단독사고 범위 확대. from 보상범위 확대 또는 한정
    limitRangeSelfCar?: boolean; // 차대차 및 도난사고 한정. from 보상범위 확대 또는 한정
    limitUninsuredCar?: boolean; // 무보험자동차에 의한 사고 한정 보상
    sevenYearsUpperForeignCar?: boolean; // 자차 보상한도 확대
    transportationCost?: boolean; // 대체교통비 지금. from 대체교통비 또는 렌트비용 선택
    rentCarCost?: boolean; // 렌트비용+렌터카 차량손해(손해담보). from 대체교통비 또는 렌트비용 선택
    leisureGoods?: boolean; // 레저용품 손해보상
    newCarCost?: boolean; // 신차가액보상
    devaluateCarCost?: boolean; // 자동차시세하락 손해보상
    imprintSupport?: boolean; // 취등록세 지원
    transportCarSupport?: boolean; // 사고차량 운반지원
    // 개발코드에서 json[key] 이게 빨간색 떠서 아래와 같이 넣어준다.
    [key: string]: any;
}

export interface InputMatching {
    subAttributes: SubAttributes;
    // 대인2 속성
    legalSupport: boolean; // 형사상 책임 포괄
    // 대물 속성
    foreignCarAccident: boolean; // 외산차 사고

    // 무보험 속성
    driveOtherCar: boolean; // 다른 자동차 운전 확대.
    driverRangeOtherCar: boolean; // 다른 차 보상에 대한 운전자범위 확대.
    damageOtherCar: boolean; // 다른 차 차량손해.
    substitutiveDriver: boolean; // 대리운전 운전담보.
    damageByOtherCar: boolean; // 다른 차에 의한 사고에 대해 보상확대

    // 자기차량손해
    expandRangeSelfCar: boolean; // 차량단독사고 범위 확대. from 보상범위 확대 또는 한정
    limitRangeSelfCar: boolean; // 차대차 및 도난사고 한정. from 보상범위 확대 또는 한정
    limitUninsuredCar: boolean; // 무보험자동차에 의한 사고 한정 보상
    sevenYearsUpperForeignCar: boolean; // 자차 보상한도 확대
    transportationCost: boolean; // 대체교통비 지금. from 대체교통비 또는 렌트비용 선택
    rentCarCost: boolean; // 렌트비용+렌터카 차량손해(손해담보). from 대체교통비 또는 렌트비용 선택
    leisureGoods: boolean; // 레저용품 손해보상
    newCarCost: boolean; // 신차가액보상
    devaluateCarCost: boolean; // 자동차시세하락 손해보상
    imprintSupport: boolean; // 취등록세 지원
    transportCarSupport: boolean; // 사고차량 운반지원
    // 개발코드에서 json[key] 이게 빨간색 떠서 아래와 같이 넣어준다.
    [key: string]: any;
}

export interface InputAttribute {
    coverage: InputCoverage;
    matchingAttribute?: InputMatching;
    condition?: InputCondition;
}

export interface InputCondition {
    person2?: boolean; // 대인2
    bodyAccident?: boolean; // 자기신체사고
    carAccident?: boolean; // 자동차상해
    uninsuredCar?: boolean; // 무보험
    selfCar?: boolean; // 자기차량손해

    // 운전자범위
    me?: boolean; // 본인
    couple?: boolean; // 배우자
    children?: boolean; // 자녀
    parents?: boolean; // 부모
    anyone?: boolean; // 누구나
    designatePerson?: boolean; // 지정 1인

    // 전기차; 외산차; 국내차
    domesticCar?: boolean;
    foreignCar?: boolean;
    electricCar?: boolean;
    // 연식
    sixMonthsBelow?: boolean;
    twelveMonthsBelow?: boolean;
    threeYearsBelow?: boolean;
    fiveYearsBelow?: boolean;
    sevenYearsBelow?: boolean;
    sevenYearsUpper?: boolean;

    // 개발코드에서 json[key] 이게 빨간색 떠서 아래와 같이 넣어준다.
    [key: string]: any;
}

export interface Condition {
    // 대인2
    person2: boolean;
    // accident 자손/자상
    accident: {
        isAnd: boolean;
        condition: {
            bodyAccident: boolean; // 자기신체사고
            carAccident: boolean; // 자동차상해
        }
    };
    uninsuredCar: boolean;
    selfCar: boolean;
    member: {
        me?: boolean; // 본인
        couple?: boolean; // 배우자
        children?: boolean; // 자녀
        parents?: boolean; // 부모
        anyone?: boolean; // 누구나
        designatePerson?: boolean; // 지정 1인
    };

    domesticCar: boolean; // 국산차
    foreignCar: boolean; // 외제차
    electricCar: boolean; // 전기차

    // 연식
    sixMonthsBelow: boolean;
    twelveMonthsBelow: boolean;
    threeYearsBelow: boolean;
    fiveYearsBelow: boolean;
    sevenYearsBelow: boolean;
    sevenYearsUpper: boolean;
}