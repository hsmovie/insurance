export interface InputCoverage {
    // 대인2
    person2?: { title: string; value: number; };
    // 대물
    object?: { title: string; value: number; };
    accident?: {
        isCar: boolean;
        death: { title: string; value: number; }; // 사망
        injury: { title: string; value: number; }; // 부상
    };
    // 무보험
    uninsuredCar?: { title: string; value: number; };
    // 물적할증금액
    surcharge?: { title: string; value: number; };
    // 자기차량손해
    selfCar?: {
        rate: { title: string; value: number; };
        min: { title: string; value: number; };
        max: { title: string; value: number; };
    };

    [key: string]: any;
}