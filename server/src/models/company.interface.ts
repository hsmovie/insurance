import { InputCondition, InputMatching } from './attribute.interface';
import { InputCoverage } from './coverage.interface';
import { Dispatch } from './dispatch.interface';

export interface InputData {
    coverage: InputCoverage;
    matchingAttribute?: InputMatching;
    condition?: InputCondition;
    dispatch?: Dispatch;
}