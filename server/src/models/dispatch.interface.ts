export interface Dispatch {
    dispatch: {
        title: string;
        value: number;
    };
    extraService: boolean; // 차량진단 등 부가서비스 제공
    unlockService: boolean; // 잠금장치해체, 배터리 충전 서비스 제외
    electricCar: boolean; // 전기차 대상 대체특약
}
