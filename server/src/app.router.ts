/**
 * Created by mayajuni on 2016. 11. 22..
 */
import { Application } from 'express';
import { PackagesRouter } from './router/packages/packages.router';
import { SubAttributesRouter } from './router/sub-attributes/sub-attributes.router';
import { DashboardRouter } from './router/dashboard/dashboard.router';
import { CoverageRouter } from './router/coverage/coverage.router';
import { ItemRouter } from './router/item/item.router';

export class AppRouter {
    private _app: Application;

    constructor(app: Application) {
        this._app = app;
        this._router();
    }

    // 여기에 router를 추가 하면 된다.
    private _router() {
        PackagesRouter.create(this._app);
        SubAttributesRouter.create(this._app);
        DashboardRouter.create(this._app);
        CoverageRouter.create(this._app);
        ItemRouter.create(this._app);
    }
}
