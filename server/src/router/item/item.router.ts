import { Application, Router } from 'express';
import { wrap } from '../../module/utils.module';
import { ItemService } from '../../service/router/item.service';
import { validation } from '../../module/validation.module';
import { getItemSchema, saveItemSchema } from './item.json-schema';

export class ItemRouter {
    router: Router;

    private _itemService: ItemService;

    /**
     * 라우터 선언
     */
    constructor() {
        this.router = Router();
        this._itemService = new ItemService();
        this._router();
    }

    /**
     * room 라우터들
     *
     * @private
     */
    private _router() {
        this.router.post('/', validation(saveItemSchema), wrap(async (req: any, res: any) => {
            const msg = await this._itemService.save(req.body.id, req.body.description);
            res.send(msg);
        }));
        this.router.get('/', validation(getItemSchema), wrap(async (req: any, res: any) => {
            const msg = await this._itemService.get(req.query.company);
            res.send(msg);
        }));
        this.router.get('/test', wrap(async (req: any, res: any) => {
            const msg = await this._itemService.getTest(req.query.id);
            res.send(msg);
        }));
    }

    public static create(app: Application) {
        const router = new ItemRouter();
        app.use('/api/item', router.router);
    }
}
