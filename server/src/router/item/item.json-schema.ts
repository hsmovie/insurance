export const saveItemSchema = {
    description: 'item save schema',
    type: 'object',
    properties: {
        id: {
            description: 'rethink db id',
            type: 'string'
        },
        description: {
            description: 'description',
            type: 'string'
        },
    },
    required: ['id', 'description'],
};

export const getItemSchema = {
    description: 'item get schema',
    type: 'object',
    properties: {
        company: {
            description: 'company',
            type: 'string'
        }
    },
    required: ['company'],
};

