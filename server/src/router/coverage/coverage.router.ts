import { Application, Router } from 'express';
import { CoverageService } from '../../service/router/coverage.service';
import { validation } from '../../module/validation.module';
import { wrap } from '../../module/utils.module';

export class CoverageRouter {
    router: Router;

    private _coverageService: CoverageService;

    /**
     * 라우터 선언
     */
    constructor() {
        this.router = Router();
        this._coverageService = new CoverageService();
        this._router();
    }

    /**
     * room 라우터들
     *
     * @private
     */
    private _router() {
        this.router.get('/', wrap(async (req: any, res: any) => {
            const msg = await this._coverageService.get();
            res.send(msg);
        }));
    }

    public static create(app: Application) {
        const router = new CoverageRouter();
        app.use('/api/coverage', router.router);
    }
}
