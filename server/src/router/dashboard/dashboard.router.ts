import { Application, Router } from 'express';
import { dashboardSchema } from './dashboard.json-schema';
import { validation } from '../../module/validation.module';
import { DashboardService } from '../../service/router/dashboard.service';
import { wrap } from '../../module/utils.module';

export class DashboardRouter {
    router: Router;

    private _dashboardService: DashboardService;

    /**
     * 라우터 선언
     */
    constructor() {
        this.router = Router();
        this._dashboardService = new DashboardService();
        this._router();
    }

    /**
     * room 라우터들
     *
     * @private
     */
    private _router() {
        this.router.post('/', validation(dashboardSchema), wrap(async (req: any, res: any) => {
            const msg = await this._dashboardService.get(req.body);
            res.send(msg);
        }));
    }

    public static create(app: Application) {
        const router = new DashboardRouter();
        app.use('/api/dashboard', router.router);
    }
}
