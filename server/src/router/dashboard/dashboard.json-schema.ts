export const dashboardSchema = {
    description: 'dashboard schema',
    type: 'object',
    properties: {
        dispatch: {
            type: 'object',
            dispatch: { type: 'object' },
            required: ['dispatch']
        }
    },
    required: ['dispatch'],
};
