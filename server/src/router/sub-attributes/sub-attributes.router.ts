import { Application, Router } from 'express';
import { SubAttributesService } from '../../service/router/sub-attributes.service';
import { validation } from '../../module/validation.module';
import { wrap } from '../../module/utils.module';

export class SubAttributesRouter {
    router: Router;

    private _subAttributesService: SubAttributesService;

    /**
     * 라우터 선언
     */
    constructor() {
        this.router = Router();
        this._subAttributesService = new SubAttributesService();
        this._router();
    }

    /**
     * room 라우터들
     *
     * @private
     */
    private _router() {
        this.router.get('/', wrap(async (req: any, res: any) => {
            const msg = await this._subAttributesService.get();
            res.send(msg[0]);
        }));
    }

    public static create(app: Application) {
        const router = new SubAttributesRouter();
        app.use('/api/subAttributes', router.router);
    }
}
