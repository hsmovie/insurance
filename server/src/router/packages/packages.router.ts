import { Application, Router } from 'express';
import { PackagesService } from '../../service/router/packages.service';
import { validation } from '../../module/validation.module';
import { wrap } from '../../module/utils.module';

export class PackagesRouter {
    router: Router;

    private _packagesService: PackagesService;

    /**
     * 라우터 선언
     */
    constructor() {
        this.router = Router();
        this._packagesService = new PackagesService();
        this._router();
    }

    /**
     * room 라우터들
     *
     * @private
     */
    private _router() {
        this.router.get('/', wrap(async (req: any, res: any) => {
            const msg = await this._packagesService.get();
            res.send(msg);
        }));
    }

    public static create(app: Application) {
        const router = new PackagesRouter();
        app.use('/api/packages', router.router);
    }
}
