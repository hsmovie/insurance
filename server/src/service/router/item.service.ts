import { RethinkDB } from '../../module/rethinkdb.module';
import { Config } from '../../config/config';

export class ItemService {
    private _r: any;

    constructor() {
        this._r = RethinkDB.r;
    }

    save = async (id: string, description: string) => await this._r.table(Config.TABLE_ITEMS).get(id).update({description});
    get = async (company: string) => await this._r.table(Config.TABLE_ITEMS).filter({company});
    getTest = async (id: string) => await this._r.table(Config.TABLE_ITEMS).get(id);
}
