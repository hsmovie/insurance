import { RethinkDB } from '../../module/rethinkdb.module';
import { Config } from '../../config/config';

export class CoverageService {
  private _r: any;
  constructor() {
      this._r = RethinkDB.r;
  }

 get = async () => (await this._r.table(Config.TABLE_COVERAGE))[0];
}
