import { DispatchService } from '../core/dispatch.service';
import { SamsungService } from '../company/samsung.service';
import { AxaService } from '../company/axa.service';
import { DongbuService } from '../company/dongbu.service';
import { HanwhaService } from '../company/hanwha.service';
import { HeungkukService } from '../company/heungkuk.service';
import { KbService } from '../company/kb.service';
import { LotteService } from '../company/lotte.service';
import { MeritzService } from '../company/meritz.service';
import { MgService } from '../company/mg.service';
import { TheKService } from '../company/theK.service';
import { InputData } from '../../models/company.interface';
import { HyundaiService } from '../company/hyundai.service';

export class DashboardService {
    private _samsungService: SamsungService;
    private _axaService: AxaService;
    private _dongbuService: DongbuService;
    private _hanwhaService: HanwhaService;
    private _heungkukService: HeungkukService;
    private _kbService: KbService;
    private _lotteService: LotteService;
    private _meritzService: MeritzService;
    private _mgService: MgService;
    private _theKService: TheKService;
    private _dispatchService: DispatchService;
    private _hyundaiService: HyundaiService;

    constructor() {
        this._samsungService = new SamsungService();
        this._axaService = new AxaService();
        this._dongbuService = new DongbuService();
        this._hanwhaService = new HanwhaService();
        this._heungkukService = new HeungkukService();
        this._kbService = new KbService();
        this._lotteService = new LotteService();
        this._meritzService = new MeritzService();
        this._mgService = new MgService();
        this._theKService = new TheKService();
        this._dispatchService = new DispatchService();
        this._hyundaiService = new HyundaiService();
    }

    async get(inputData: InputData) {
        const result: any = {};
        result['samsung'] = await this._samsungService.get(inputData);
        result['hyundai'] = await this._hyundaiService.get(inputData);
        result['axa'] = await this._axaService.get(inputData);
        result['db'] = await this._dongbuService.get(inputData);
        result['hanwha'] = await this._hanwhaService.get(inputData);
        result['heungkuk'] = await this._heungkukService.get(inputData);
        result['kb'] = await this._kbService.get(inputData);
        result['lotte'] = await this._lotteService.get(inputData);
        result['meritz'] = await this._meritzService.get(inputData);
        result['mg'] = await this._mgService.get(inputData);
        result['thek'] = await this._theKService.get(inputData);

        return result;
    }
}
