import { AttributesService } from '../core/attributes.service';
import { InputAttribute, Item, ItemBuyItemGroups, ItemGroupsByAttribute } from '../../models/attribute.interface';
import { RethinkDB } from '../../module/rethinkdb.module';
import { InputData } from '../../models/company.interface';
import { CoverageService } from '../core/coverage.service';
import { DispatchService } from '../core/dispatch.service';

export class LotteService {
    private _attributesService: AttributesService;
    private _coverageService: CoverageService;
    private _dispatchService: DispatchService;
    private _autoAddItems: Item[];
    private _r: any;
    private _company: string;

    constructor() {
        this._r = RethinkDB.r;
        this._company = '롯데손해보험';
        this._attributesService = new AttributesService();
        this._coverageService = new CoverageService();
        this._dispatchService = new DispatchService();
    }

    async get(inputData: InputData) {
        const matchingItem = await this._getAttributes(inputData);
        const coverage = await this._coverageService.get(this._company, inputData.coverage);
        const dispatch = this._dispatchService.get(this._company, inputData.dispatch);
        return {
            matchingItem,
            coverage,
            dispatch
        };
    }

    private async _getAttributes(inputData: InputAttribute) {
        this._autoAddItems = [];
        const attributes: ItemGroupsByAttribute = await this._attributesService.getItems(this._company, inputData);
        let items: ItemBuyItemGroups = await this._condition(attributes, inputData);
        await this._exceptionHandling(inputData);
        const result = {
            matchingItem: items,
            autoAddItems: this._autoAddItems
        };

        return result;
    }

    /**
     * 자상/자손 자차에 맞는 가입조건을 컨트롤 한다.
     *
     * 자동 가입조건
     *
     * @param {SubAttributeItems} itemGroups
     * @private
     */
    private async _condition(itemGroups: ItemGroupsByAttribute, inputData: InputAttribute): Promise<ItemBuyItemGroups> {
        const itemGroupKeys = Object.keys(itemGroups);
        const inputCondition = inputData.condition;

        let result = [];

        for (const itemGroupKey of itemGroupKeys) {
            const combination = itemGroups[itemGroupKey];
            const combinationItems = combination.items;
            let checkCombinationItem = true;
            let hasNo7 = false;
            let hasNo12 = false;
            let hasNo13 = false;
            let hasNo14 = false;
            let hasNo15 = false;
            let hasNo16 = false;
            let hasNo17 = false;

            for (const item of combinationItems) {
                const condition = item.condition;
                checkCombinationItem = this._attributesService.checkExceptionCondition(condition, inputCondition, item);
                if (!checkCombinationItem) {
                    break;
                }

                const title = item.title;
                if (title === '출퇴근안심 특별약관') {
                    hasNo7 = true;
                } else if (title === '교통상해 담보 Ⅱ 특별약관 (개인형)') {
                    hasNo12 = true;
                } else if (title === '교통상해 담보 Ⅱ 특별약관 (부부형)') {
                    hasNo13 = true;
                } else if (title === '주말 교통상해 담보Ⅱ 특별약관 (개인형)') {
                    hasNo14 = true;
                } else if (title === '주말 교통상해 담보Ⅱ 특별약관 (부부형)') {
                    hasNo15 = true;
                } else if (title === '주일안심 특별약관') {
                    hasNo16 = true;
                } else if (title === '새벽안심 특별약관') {
                    hasNo17 = true;
                }
            }

            if (checkCombinationItem) {
                if (hasNo7 && (hasNo14 || hasNo15 || hasNo16 || hasNo17)) {
                    checkCombinationItem = false;
                } else if (hasNo12 && (hasNo13 || hasNo14 || hasNo15)) {
                    checkCombinationItem = false;
                } else if (hasNo13 && (hasNo12 || hasNo14 || hasNo15)) {
                    checkCombinationItem = false;
                } else if (hasNo14 && (hasNo12 || hasNo13 || hasNo15)) {
                    checkCombinationItem = false;
                } else if (hasNo15 && (hasNo12 || hasNo13 || hasNo14)) {
                    checkCombinationItem = false;
                } else if (hasNo16 || hasNo17) {
                    const inputAccident = inputData.coverage.accident;
                    if (inputAccident) {
                        if (!inputAccident.isCar && inputAccident.death.value > 50000000) {
                            checkCombinationItem = false;
                        }
                    } else {
                        checkCombinationItem = false;
                    }
                }
            }

            // 스코어 순서이기 떄문에 빠르게 나온걸 items에 넣고 리턴한다.
            if (checkCombinationItem) {
                result.push(combination);
            }
        }
        result = result
            .sort((a, b) =>
                (a.score.overCount + a.score.sameCount + a.score.needCount) >
                (b.score.overCount + b.score.sameCount + b.score.needCount) ? 1
                    : (a.score.overCount + a.score.sameCount + a.score.needCount) <
                    (b.score.overCount + b.score.sameCount + b.score.needCount) ? -1 : 0
                    || a.groupSize > b.groupSize ? -1 : a.groupSize < b.groupSize ? 1 : 0);

        return result[0];
    }

    private async _exceptionHandling(inputData: InputAttribute) {
        const matchingAttribute = inputData.matchingAttribute;
        const condition = inputData.condition;
        // 이건 인풋 데이터의 가입조건에 따라 공통 사항
        if (condition.uninsuredCar) {
            this._autoAddItems.push(await this._attributesService.getItemInfo(this._company, '다른 자동차 운전담보 특별약관'));
        }

        if (condition.carAccident && matchingAttribute.damageByOtherCar) {
            this._autoAddItems.push(await this._attributesService.getItemInfo(this._company, '자동차상해Ⅱ 특별약관'));
        }

        if (matchingAttribute.expandRangeSelfCar &&
            (matchingAttribute.imprintSupport ||
                matchingAttribute.transportCarSupport ||
                matchingAttribute.transportationCost)) {
            this._autoAddItems.push(await this._attributesService.getItemInfo(this._company, '플러스자기차량손해 특별약관'));
            this._autoAddItems.push(await this._attributesService.getItemInfo(this._company, '차량단독사고 보장 특별약관 II'));
        } else if (matchingAttribute.expandRangeSelfCar) {
            this._autoAddItems.push(await this._attributesService.getItemInfo(this._company, '차량단독사고 보장 특별약관 I'));

        } else if (matchingAttribute.imprintSupport ||
            matchingAttribute.transportCarSupport ||
            matchingAttribute.transportationCost) {
            this._autoAddItems.push(await this._attributesService.getItemInfo(this._company, '플러스자기차량손해 특별약관'));
        }
    }
}