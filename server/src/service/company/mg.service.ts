import { AttributesService } from '../core/attributes.service';
import { InputAttribute, Item, ItemBuyItemGroups, ItemGroupsByAttribute } from '../../models/attribute.interface';
import { RethinkDB } from '../../module/rethinkdb.module';
import { InputData } from '../../models/company.interface';
import { CoverageService } from '../core/coverage.service';
import { DispatchService } from '../core/dispatch.service';

export class MgService {
    private _attributesService: AttributesService;
    private _coverageService: CoverageService;
    private _dispatchService: DispatchService;
    private _autoAddItems: Item[];
    private _r: any;
    private _company: string;

    constructor() {
        this._r = RethinkDB.r;
        this._company = 'MG손해보험';
        this._attributesService = new AttributesService();
        this._coverageService = new CoverageService();
        this._dispatchService = new DispatchService();
    }

    async get(inputData: InputData) {
        const matchingItem = await this._getAttributes(inputData);
        const coverage = await this._coverageService.get(this._company, inputData.coverage);
        const dispatch = this._dispatchService.get(this._company, inputData.dispatch);
        return {
            matchingItem,
            coverage,
            dispatch
        };
    }

    private async _getAttributes(inputData: InputAttribute) {
        this._autoAddItems = [];
        const attributes: ItemGroupsByAttribute = await this._attributesService.getItems(this._company, inputData);
        let items: ItemBuyItemGroups = await this._condition(attributes, inputData);
        await this._exceptionHandling(inputData);
        const result = {
            matchingItem: items,
            autoAddItems: this._autoAddItems
        };

        return result;
    }

    /**
     * 자상/자손 자차에 맞는 가입조건을 컨트롤 한다.
     *
     * 자동 가입조건
     *
     * @param {SubAttributeItems} itemGroups
     * @private
     */
    private async _condition(itemGroups: ItemGroupsByAttribute, inputData: InputAttribute): Promise<ItemBuyItemGroups> {
        const itemGroupKeys = Object.keys(itemGroups);
        const inputCondition = inputData.condition;

        let result = [];

        for (const itemGroupKey of itemGroupKeys) {
            const combination = itemGroups[itemGroupKey];
            const combinationItems = combination.items;

            let checkCombinationItem = true;
            for (const item of combinationItems) {
                const condition = item.condition;
                checkCombinationItem = this._attributesService.checkExceptionCondition(condition, inputCondition, item);
                if (!checkCombinationItem) {
                    break;
                }
            }

            // 스코어 순서이기 떄문에 빠르게 나온걸 items에 넣고 리턴한다.
            if (checkCombinationItem) {
                result.push(combination);
            }
        }
        result = result
            .sort((a, b) =>
                (a.score.overCount + a.score.sameCount + a.score.needCount) >
                (b.score.overCount + b.score.sameCount + b.score.needCount) ? 1
                    : (a.score.overCount + a.score.sameCount + a.score.needCount) <
                    (b.score.overCount + b.score.sameCount + b.score.needCount) ? -1 : 0
                    || a.groupSize > b.groupSize ? -1 : a.groupSize < b.groupSize ? 1 : 0);

        return result[0];
    }

    private async _exceptionHandling(inputData: InputAttribute) {
        const matchingAttribute = inputData.matchingAttribute;
        const condition = inputData.condition;
        // 이건 인풋 데이터의 가입조건에 따라 공통 사항
        if (condition.selfCar &&
            (matchingAttribute.expandRangeSelfCar ||
                matchingAttribute.imprintSupport ||
                matchingAttribute.transportCarSupport)) {
            if (!matchingAttribute.transportationCost) {
                this._autoAddItems.push(await this._attributesService.getItemInfo(this._company, '자기차량손해 알파'));
            } else if (matchingAttribute.transportationCost) {
                this._autoAddItems.push(await this._attributesService.getItemInfo(this._company, '자기차량손해 플러스'));
            }
        }
    }
}