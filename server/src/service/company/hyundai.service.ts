import { AttributesService } from '../core/attributes.service';
import { InputAttribute, Item, ItemBuyItemGroups, ItemGroupsByAttribute } from '../../models/attribute.interface';
import { RethinkDB } from '../../module/rethinkdb.module';
import { InputData } from '../../models/company.interface';
import { CoverageService } from '../core/coverage.service';
import { DispatchService } from '../core/dispatch.service';

export class HyundaiService {
    private _attributesService: AttributesService;
    private _coverageService: CoverageService;
    private _dispatchService: DispatchService;
    private _autoAddItems: Item[];
    private _r: any;
    private _company: string;

    constructor() {
        this._r = RethinkDB.r;
        this._company = '현대해상화재보험';
        this._attributesService = new AttributesService();
        this._coverageService = new CoverageService();
        this._dispatchService = new DispatchService();
    }

    async get(inputData: InputData) {
        const matchingItem = await this._getAttributes(inputData);
        const coverage = await this._coverageService.get(this._company, inputData.coverage);
        const dispatch = this._dispatchService.get(this._company, inputData.dispatch);
        return {
            matchingItem,
            coverage,
            dispatch
        };
    }

    private async _getAttributes(inputData: InputAttribute) {
        this._autoAddItems = [];
        const attributes: ItemGroupsByAttribute = await this._attributesService.getItems(this._company, inputData);
        let items: ItemBuyItemGroups = await this._condition(attributes, inputData);
        await this._exceptionHandling(inputData);
        const result = {
            matchingItem: items,
            autoAddItems: this._autoAddItems
        };

        return result;
    }

    /**
     * 자상/자손 자차에 맞는 가입조건을 컨트롤 한다.
     *
     * 자동 가입조건
     *
     * @param {SubAttributeItems} attributes
     * @private
     */
    private async _condition(itemGroups: ItemGroupsByAttribute, inputData: InputAttribute): Promise<ItemBuyItemGroups> {
        const itemGroupKeys = Object.keys(itemGroups);
        const inputCondition = inputData.condition;

        let result = [];

        for (const itemGroupKey of itemGroupKeys) {
            const combination = itemGroups[itemGroupKey];
            const combinationItems = combination.items;

            let checkCombinationItem = true;
            // 엑셀 기준 18번 특약이 있는지 체크
            let hasNo18 = false;
            let hasNo19 = false;
            let hasNo13 = false;
            let hasNo14 = false;
            let hasNo15 = false;
            let hasNo17 = false;
            let hasNo16 = false;

            for (const item of combinationItems) {
                const condition = item.condition;
                checkCombinationItem = this._attributesService.checkExceptionCondition(condition, inputCondition, item);
                if (!checkCombinationItem) {
                    break;
                }
                const title = item.title;
                // 중복 가입 불가 혹은 무언가 필수 가입을 체크 하기 위해서 title만 가지는 변수를 만들었다.
                if (title === '신차손해담보 특별약관') {
                    hasNo18 = true;
                } else if (title === '자기차량손해 보장 확대 특별약관') {
                    hasNo13 = true;
                } else if (title === '자동차시세 하락손해 지원담보 특별약관') {
                    hasNo14 = true;
                } else if (title === '전손 시 대체차량 등록비용담보 특별약관') {
                    hasNo15 = true;
                } else if (title === '자녀 운1전담보 추가특별약관/다른 자동차 차량손해담보 특별약관') {
                    hasNo17 = true;
                } else if (title === '다른 자동차 차량손해담보 특별약관(차대차 충돌사고 한정)') {
                    hasNo16 = true;
                } else if (title === '렌터카 차량손해담보 특별약관') {
                    hasNo19 = true;
                }
            }

            if (checkCombinationItem) {
                // 18번은 14번 또는 15번 특약관 중복 되면 안된다.
                if (hasNo18 && (hasNo14 || hasNo15)) {
                    checkCombinationItem = false;
                }
                // 17번 특약이 있으면 16번 특약이 꼭 있어야된다.
                else if (hasNo17 && !hasNo16) {
                    checkCombinationItem = false;
                }
                else if ((hasNo14 || hasNo19) && !(inputData.condition.selfCar && hasNo13)) {
                    checkCombinationItem = false;
                }
            }

            // 스코어 순서이기 떄문에 빠르게 나온걸 items에 넣고 리턴한다.
            if (checkCombinationItem) {
                result.push(combination);
            }
        }
        result = result
            .sort((a, b) =>
                (a.score.overCount + a.score.sameCount + a.score.needCount) >
                (b.score.overCount + b.score.sameCount + b.score.needCount) ? 1
                    : (a.score.overCount + a.score.sameCount + a.score.needCount) <
                    (b.score.overCount + b.score.sameCount + b.score.needCount) ? -1 : 0
                    || a.groupSize > b.groupSize ? -1 : a.groupSize < b.groupSize ? 1 : 0);
        return result[0];
    }

    private async _exceptionHandling(inputData: InputAttribute) {
        // 이건 인풋 데이터의 가입조건에 따라 공통 사항
        if (inputData.condition.uninsuredCar) {
            const autoItem = await this._attributesService.getItemInfo(this._company, '다른 자동차 운전담보 특별약관');
            this._autoAddItems.push(autoItem);
        }
    }
}