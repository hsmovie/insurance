import { AttributesService } from '../core/attributes.service';
import { InputAttribute, Item, ItemBuyItemGroups, ItemGroupsByAttribute } from '../../models/attribute.interface';
import { RethinkDB } from '../../module/rethinkdb.module';
import { InputData } from '../../models/company.interface';
import { CoverageService } from '../core/coverage.service';
import { DispatchService } from '../core/dispatch.service';

export class DongbuService {
    private _attributesService: AttributesService;
    private _coverageService: CoverageService;
    private _dispatchService: DispatchService;
    private _autoAddItems: Item[];
    private _r: any;
    private _company: string;

    constructor() {
        this._r = RethinkDB.r;
        this._company = '동부화재';
        this._attributesService = new AttributesService();
        this._coverageService = new CoverageService();
        this._dispatchService = new DispatchService();
    }

    async get(inputData: InputData) {
        const matchingItem = await this._getAttributes(inputData);
        const coverage = await this._coverageService.get(this._company, inputData.coverage);
        const dispatch = this._dispatchService.get(this._company, inputData.dispatch);
        return {
            matchingItem,
            coverage,
            dispatch
        };
    }

    private async _getAttributes(inputData: InputAttribute) {
        this._autoAddItems = [];
        const attributes: ItemGroupsByAttribute = await this._attributesService.getItems(this._company, inputData);
        let items: ItemBuyItemGroups = await this._condition(attributes, inputData);
        await this._exceptionHandling(inputData);
        const result = {
            matchingItem: items,
            autoAddItems: this._autoAddItems
        };

        return result;
    }

    /**
     * 자상/자손 자차에 맞는 가입조건을 컨트롤 한다.
     *
     * 자동 가입조건
     *
     * @param {SubAttributeItems} attributes
     * @private
     */
    private async _condition(itemGroups: ItemGroupsByAttribute, inputData: InputAttribute): Promise<ItemBuyItemGroups> {
        const itemGroupKeys = Object.keys(itemGroups);
        const inputCondition = inputData.condition;

        let result = [];

        for (const itemGroupKey of itemGroupKeys) {
            const combination = itemGroups[itemGroupKey];
            let combinationItems = combination.items;

            let checkCombinationItem = true;

            let hasNo13 = false;
            let hasNo14 = false;
            let hasNo15 = false;
            let hasNo16 = false;
            let hasNo18 = false;
            let hasNo22 = false;
            let hasNo23 = false;
            let hasNo29 = false;
            let addFamily = false;

            for (const item of combinationItems) {
                const condition = item.condition;
                checkCombinationItem = this._attributesService.checkExceptionCondition(condition, inputCondition, item);
                if (!checkCombinationItem) {
                    break;
                }
                const title = item.title;
                // 예외처리 자동차상해 - Family 통합보장 특별약관
                if (title === '보행 중 상해 특별약관') {
                    if (inputCondition.carAccident || inputCondition.bodyAccident) {
                        addFamily = true;
                    }
                }

                if (title === '원격지 차량운반비용 담보 특별약관') {
                    hasNo13 = true;
                } else if (title === '렌트비용 담보 특별약관 [1]렌트비용담보/[2] 렌터카 손해담보') {
                    hasNo14 = true;
                } else if (title === '고장수리 시 렌터카 운전담보 추가특별약관') {
                    hasNo15 = true;
                } else if (title === '임시교통비 담보 특별약관') {
                    hasNo16 = true;
                } else if (title === '외제차 운반비용 담보 특별약관') {
                    hasNo18 = true;
                } else if (title === '다른 자동차 차량손해 특별약관') {
                    hasNo22 = true;
                } else if (title === '자녀운전자 담보 추가특별약관/다른 자동차 차량손해 특별약관') {
                    hasNo23 = true;
                } else if (title === '외제차 충돌 시 대물 보장확대 특별약관') {
                    hasNo29 = true;
                }
            }

            if (checkCombinationItem) {
                if (hasNo14 && hasNo16) {
                    checkCombinationItem = false;
                }
                else if (hasNo15 && !hasNo14) {
                    checkCombinationItem = false;
                }
                else if (hasNo13 && hasNo18) {
                    checkCombinationItem = false;
                }
                else if (hasNo23 && !hasNo22) {
                    checkCombinationItem = false;
                }
                else if (hasNo29 && (inputData.coverage.object.value <= 20000000)) {
                    checkCombinationItem = false;
                }
            }

            // 스코어 순서이기 떄문에 빠르게 나온걸 items에 넣고 리턴한다.
            if (checkCombinationItem) {
                // 기존 보행 중 상해 특별약관 삭제 후 Family 통합보장 특별약관 추가
                if (addFamily) {
                    combinationItems = combinationItems.filter(item => item.title !== '보행 중 상해 특별약관');
                    const family = await this._attributesService.getItemInfo(this._company, '자동차상해 Family 통합보장 특별약관');
                    combinationItems.push(family);
                }

                result.push(combination);
            }
        }

        result = result
            .sort((a, b) =>
                (a.score.overCount + a.score.sameCount + a.score.needCount) >
                (b.score.overCount + b.score.sameCount + b.score.needCount) ? 1
                    : (a.score.overCount + a.score.sameCount + a.score.needCount) <
                    (b.score.overCount + b.score.sameCount + b.score.needCount) ? -1 : 0
                    || a.groupSize > b.groupSize ? -1 : a.groupSize < b.groupSize ? 1 : 0);

        return result[0];
    }

    private async _exceptionHandling(inputData: InputAttribute) {
        // 이건 인풋 데이터의 가입조건에 따라 공통 사항
        if (inputData.condition.uninsuredCar) {
            const autoItem = await
                this._attributesService.getItemInfo(this._company, '다른 자동차 운전담보 특별약관');
            this._autoAddItems.push(autoItem);
        }
    }
}