import { AttributesService } from '../core/attributes.service';
import { InputAttribute, Item, ItemBuyItemGroups, ItemGroupsByAttribute } from '../../models/attribute.interface';
import { RethinkDB } from '../../module/rethinkdb.module';
import { InputData } from '../../models/company.interface';
import { CoverageService } from '../core/coverage.service';
import { DispatchService } from '../core/dispatch.service';

export class HeungkukService {
    private _attributesService: AttributesService;
    private _coverageService: CoverageService;
    private _dispatchService: DispatchService;
    private _autoAddItems: Item[];
    private _r: any;
    private _company: string;

    constructor() {
        this._r = RethinkDB.r;
        this._company = '흥국화재';
        this._attributesService = new AttributesService();
        this._coverageService = new CoverageService();
        this._dispatchService = new DispatchService();
    }

    async get(inputData: InputData) {
        const matchingItem = await this._getAttributes(inputData);
        const coverage = await this._coverageService.get(this._company, inputData.coverage);
        const dispatch = this._dispatchService.get(this._company, inputData.dispatch);
        return {
            matchingItem,
            coverage,
            dispatch
        };
    }

    private async _getAttributes(inputData: InputAttribute) {
        this._autoAddItems = [];
        const attributes: ItemGroupsByAttribute = await this._attributesService.getItems(this._company, inputData);
        let items: ItemBuyItemGroups = await this._condition(attributes, inputData);
        await this._exceptionHandling(inputData);
        const result = {
            matchingItem: items,
            autoAddItems: this._autoAddItems
        };

        return result;
    }

    /**
     * 자상/자손 자차에 맞는 가입조건을 컨트롤 한다.
     *
     * 자동 가입조건
     *
     * @param {SubAttributeItems} attributes
     * @private
     */
    private async _condition(itemGroups: ItemGroupsByAttribute, inputData: InputAttribute): Promise<ItemBuyItemGroups> {
        const itemGroupKeys = Object.keys(itemGroups);
        const inputCondition = inputData.condition;

        let result = [];

        for (const itemGroupKey of itemGroupKeys) {
            const combination = itemGroups[itemGroupKey];
            const combinationItems = combination.items;

            let checkCombinationItem = true;

            let hasNo8 = false;
            let hasNo9 = false;
            let hasNo10 = false;
            let hasNo11 = false;
            let hasNo12 = false;
            let hasNo13 = false;
            let hasNo14 = false;
            let hasNo15 = false;

            for (const item of combinationItems) {
                const condition = item.condition;
                checkCombinationItem = this._attributesService.checkExceptionCondition(condition, inputCondition, item);
                if (!checkCombinationItem) {
                    break;
                }
                const title = item.title;
                if (title === '마이키즈 LOVE(TYPEI) 특별약관') {
                    hasNo8 = true;
                } else if (title === '마이키즈 LOVE(TYPEII) 특별약관') {
                    hasNo9 = true;
                } else if (title === '차량 단독사고 손해보상 특별약관') {
                    hasNo11 = true;
                } else if (title === '자기차량사고 격락손해 및 위로금 특별약관') {
                    hasNo10 = true;
                } else if (title === '렌트카 담보 특별약관') {
                    hasNo12 = true;
                } else if (title === '대체교통비 지원금 특별약관') {
                    hasNo13 = true;
                } else if (title === '신차가액보상담보 특별약관') {
                    hasNo14 = true;
                } else if (title === '다른 자동차 차량손해 특별약관') {
                    hasNo15 = true;
                }
            }

            if (checkCombinationItem) {
                if (hasNo8 && hasNo9) {
                    checkCombinationItem = false;
                } else if ((hasNo10 || hasNo12 || hasNo13 || hasNo14 || hasNo15)
                    && !(inputData.condition.selfCar && hasNo11)) {
                    checkCombinationItem = false;
                }
            }


            // 스코어 순서이기 떄문에 빠르게 나온걸 items에 넣고 리턴한다.
            if (checkCombinationItem) {
                result.push(combination);
            }
        }
        result = result
            .sort((a, b) =>
                (a.score.overCount + a.score.sameCount + a.score.needCount) >
                (b.score.overCount + b.score.sameCount + b.score.needCount) ? 1
                    : (a.score.overCount + a.score.sameCount + a.score.needCount) <
                    (b.score.overCount + b.score.sameCount + b.score.needCount) ? -1 : 0
                    || a.groupSize > b.groupSize ? -1 : a.groupSize < b.groupSize ? 1 : 0);

        return result[0];
    }

    private async _exceptionHandling(inputData: InputAttribute) {
        // 이건 인풋 데이터의 가입조건에 따라 공통 사항
        if (inputData.condition.uninsuredCar) {
            const autoItem = await this._attributesService.getItemInfo(this._company, '다른 자동차 운전담보 특별약관');
            this._autoAddItems.push(autoItem);
        }
    }
}