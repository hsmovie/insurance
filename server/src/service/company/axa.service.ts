import { AttributesService } from '../core/attributes.service';
import { InputAttribute, Item, ItemBuyItemGroups, ItemGroupsByAttribute } from '../../models/attribute.interface';
import { RethinkDB } from '../../module/rethinkdb.module';
import { InputData } from '../../models/company.interface';
import { CoverageService } from '../core/coverage.service';
import { DispatchService } from '../core/dispatch.service';

export class AxaService {
    private _attributesService: AttributesService;
    private _coverageService: CoverageService;
    private _dispatchService: DispatchService;
    private _autoAddItems: Item[];
    private _r: any;
    private _company: string;

    constructor() {
        this._r = RethinkDB.r;
        this._company = 'AXA손해보험';
        this._attributesService = new AttributesService();
        this._coverageService = new CoverageService();
        this._dispatchService = new DispatchService();
    }

    async get(inputData: InputData) {
        const matchingItem = await this._getAttributes(inputData);
        const coverage = await this._coverageService.get(this._company, inputData.coverage);
        const dispatch = this._dispatchService.get(this._company, inputData.dispatch);
        return {
            matchingItem,
            coverage,
            dispatch
        };
    }

    private async _getAttributes(inputData: InputAttribute) {
        this._autoAddItems = [];
        const attributes: ItemGroupsByAttribute = await this._attributesService.getItems(this._company, inputData);
        let items: ItemBuyItemGroups = await this._condition(attributes, inputData);
        await this._exceptionHandling(inputData);
        const result = {
            matchingItem: items,
            autoAddItems: this._autoAddItems
        };

        return result;
    }

    /**
     * 자상/자손 자차에 맞는 가입조건을 컨트롤 한다.
     *
     * 자동 가입조건
     *
     * @param {SubAttributeItems} itemGroups
     * @private
     */
    private async _condition(itemGroups: ItemGroupsByAttribute, inputData: InputAttribute): Promise<ItemBuyItemGroups> {
        const itemGroupKeys = Object.keys(itemGroups);
        const inputCondition = inputData.condition;

        let result = [];

        for (const itemGroupKey of itemGroupKeys) {
            const combination = itemGroups[itemGroupKey];
            const combinationItems = combination.items;

            let checkCombinationItem = true;
            let hasNo1 = false;
            let hasNo2 = false;
            let hasNo3 = false;
            let hasNo4 = false;
            let hasNo5 = false;
            let hasNo6 = false;
            let hasNo7 = false;
            let hasNo8 = false;
            let hasNo17 = false;

            for (const item of combinationItems) {
                const condition = item.condition;
                checkCombinationItem = this._attributesService.checkExceptionCondition(condition, inputCondition, item);
                if (!checkCombinationItem) {
                    break;
                }
                const title = item.title;
                if (title === '운전자담보 특별약관') {
                    hasNo1 = true;
                } else if (title === '운전자담보 의료비추가 특별약관') {
                    hasNo2 = true;
                } else if (title === '주말·휴일 확대보상 특별약관') {
                    hasNo3 = true;
                } else if (title === '자기차량손해 확장담보 특별약관') {
                    hasNo4 = true;
                } else if (title === '렌트카담보 특별약관') {
                    hasNo5 = true;
                } else if (title === '신차가액보상담보 특별약관') {
                    hasNo6 = true;
                } else if (title === '신차가액보상담보 확장 특별약관') {
                    hasNo7 = true;
                } else if (title === '차량전손시 제반비용 담보 특별약관') {
                    hasNo8 = true;
                } else if (title === '무보험 자동차에 의한 상해 추가담보 특별약관') {
                    hasNo17 = true;
                }
            }

            if (checkCombinationItem) {
                if (hasNo2 && !hasNo1) {
                    checkCombinationItem = false;
                } else if (hasNo4 && (hasNo5 || hasNo8)) {
                    checkCombinationItem = false;
                } else if (hasNo6 && hasNo7) {
                    checkCombinationItem = false;
                } else if (hasNo3 && inputData.coverage.accident && inputData.coverage.accident.death.value >= 100000000) {
                    checkCombinationItem = false;
                } else if (hasNo17 && inputData.coverage.uninsuredCar && inputData.coverage.uninsuredCar.value <= 200000000) {
                    checkCombinationItem = false;
                }
            }

            if (checkCombinationItem) {
                if (!combination.autoAddItems) {
                    combination.autoAddItems = [];
                }

                if (!hasNo17 && inputData.coverage.uninsuredCar && inputData.coverage.uninsuredCar.value > 200000000) {
                    combination.autoAddItems.push(await this._attributesService.getItemInfo('AXA손해보험', '무보험 자동차에 의한 상해 추가담보 특별약관'));
                }
                result.push(combination);
            }
        }
        result = result
            .sort((a, b) =>
                (a.score.overCount + a.score.sameCount + a.score.needCount) >
                (b.score.overCount + b.score.sameCount + b.score.needCount) ? 1
                    : (a.score.overCount + a.score.sameCount + a.score.needCount) <
                    (b.score.overCount + b.score.sameCount + b.score.needCount) ? -1 : 0
                    || a.groupSize > b.groupSize ? -1 : a.groupSize < b.groupSize ? 1 : 0);

        return result[0];
    }

    private async _exceptionHandling(inputData: InputAttribute) {
        const condition = inputData.condition;
        const matchingAttribute = inputData.matchingAttribute;
        // 이건 인풋 데이터의 가입조건에 따라 공통 사항
        if (inputData.condition.uninsuredCar) {
            this._autoAddItems.push(await this._attributesService.getItemInfo('AXA손해보험', '다른 자동차 운전담보 특별약관'));
        }

        if (condition.selfCar && !matchingAttribute.expandRangeSelfCar) {
            this._autoAddItems.push(await this._attributesService.getItemInfo('AXA손해보험', '차대차충돌 및 도난위험 한정담보 특별약관'));
        }
    }
}