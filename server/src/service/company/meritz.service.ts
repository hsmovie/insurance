import { AttributesService } from '../core/attributes.service';
import { InputAttribute, Item, ItemBuyItemGroups, ItemGroupsByAttribute } from '../../models/attribute.interface';
import { RethinkDB } from '../../module/rethinkdb.module';
import { InputData } from '../../models/company.interface';
import { CoverageService } from '../core/coverage.service';
import { DispatchService } from '../core/dispatch.service';

export class MeritzService {
    private _attributesService: AttributesService;
    private _coverageService: CoverageService;
    private _dispatchService: DispatchService;
    private _autoAddItems: Item[];
    private _r: any;
    private _company: string;

    constructor() {
        this._r = RethinkDB.r;
        this._company = '메리츠화재보험';
        this._attributesService = new AttributesService();
        this._coverageService = new CoverageService();
        this._dispatchService = new DispatchService();
    }

    async get(inputData: InputData) {
        const matchingItem = await this._getAttributes(inputData);
        const coverage = await this._coverageService.get(this._company, inputData.coverage);
        const dispatch = this._dispatchService.get(this._company, inputData.dispatch);
        return {
            matchingItem,
            coverage,
            dispatch
        };
    }

    private async _getAttributes(inputData: InputAttribute) {
        this._autoAddItems = [];
        const attributes: ItemGroupsByAttribute = await this._attributesService.getItems(this._company, inputData);
        let items: ItemBuyItemGroups = await this._condition(attributes, inputData);
        await this._exceptionHandling(inputData);
        const result = {
            matchingItem: items,
            autoAddItems: this._autoAddItems
        };

        return result;
    }

    /**
     * 자상/자손 자차에 맞는 가입조건을 컨트롤 한다.
     *
     * 자동 가입조건
     *
     * @param {SubAttributeItems} itemGroups
     * @private
     */
    private async _condition(itemGroups: ItemGroupsByAttribute, inputData: InputAttribute): Promise<ItemBuyItemGroups> {
        const itemGroupKeys = Object.keys(itemGroups);
        const inputCondition = inputData.condition;

        let result = [];

        for (const itemGroupKey of itemGroupKeys) {
            const combination = itemGroups[itemGroupKey];
            const combinationItems = combination.items;


            let checkCombinationItem = true;
            let hasNo17 = false;
            let hasNo18 = false;
            let hasNo19 = false;
            let hasNo20 = false;
            let hasNo21 = false;
            let hasNo22 = false;
            let hasNo23 = false;

            for (const item of combinationItems) {
                const condition = item.condition;
                checkCombinationItem = this._attributesService.checkExceptionCondition(condition, inputCondition, item);
                if (!checkCombinationItem) {
                    break;
                }

                const title = item.title;
                if (title === '다른 자동차 차량손해 특별약관') {
                    hasNo17 = true;
                } else if (title === '자기차량손해 포괄담보 특별약관') {
                    hasNo18 = true;
                } else if (title === '외제차 운반비용 특별약관') {
                    hasNo19 = true;
                } else if (title === '렌트 및 대체교통비용 지원금 특별약관') {
                    hasNo20 = true;
                } else if (title === '대여자동차 자기차량손해담보 특별약관') {
                    hasNo21 = true;
                } else if (title === '차량전손시 제반비용담보 특별약관') {
                    hasNo22 = true;
                } else if (title === '차량신가보상지원 특별약관') {
                    hasNo23 = true;
                }
            }

            if (checkCombinationItem) {
                if ((hasNo17 || hasNo19 || hasNo20 || hasNo21 || hasNo22 || hasNo23)
                    && !(inputData.condition.selfCar || hasNo18)) {
                    checkCombinationItem = false;
                }
            }

            if (checkCombinationItem) {
                if (!combination.autoAddItems) {
                    combination.autoAddItems = [];
                }
                // 자동 가입
                if (hasNo20) {
                    combination.autoAddItems.push(await this._attributesService.getItemInfo(this._company, '대여자동차 자기차량손해담보 특별약관'));
                }
                result.push(combination);
            }
        }
        result = result
            .sort((a, b) =>
                (a.score.overCount + a.score.sameCount + a.score.needCount) >
                (b.score.overCount + b.score.sameCount + b.score.needCount) ? 1
                    : (a.score.overCount + a.score.sameCount + a.score.needCount) <
                    (b.score.overCount + b.score.sameCount + b.score.needCount) ? -1 : 0
                    || a.groupSize > b.groupSize ? -1 : a.groupSize < b.groupSize ? 1 : 0);

        return result[0];
    }

    private async _exceptionHandling(inputData: InputAttribute) {
        // 이건 인풋 데이터의 가입조건에 따라 공통 사항
        if (inputData.condition.uninsuredCar) {
            this._autoAddItems.push(await this._attributesService.getItemInfo(this._company, '다른 자동차 운전담보 특별약관'));
        }
    }
}