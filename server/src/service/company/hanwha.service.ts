import { AttributesService } from '../core/attributes.service';
import { InputAttribute, Item, ItemBuyItemGroups, ItemGroupsByAttribute } from '../../models/attribute.interface';
import { RethinkDB } from '../../module/rethinkdb.module';
import { InputData } from '../../models/company.interface';
import { CoverageService } from '../core/coverage.service';
import { DispatchService } from '../core/dispatch.service';

export class HanwhaService {
    private _attributesService: AttributesService;
    private _coverageService: CoverageService;
    private _dispatchService: DispatchService;
    private _autoAddItems: Item[];
    private _r: any;
    private _company: string;

    constructor() {
        this._r = RethinkDB.r;
        this._company = '한화손해보험';
        this._attributesService = new AttributesService();
        this._coverageService = new CoverageService();
        this._dispatchService = new DispatchService();
    }

    async get(inputData: InputData) {
        const matchingItem = await this._getAttributes(inputData);
        const coverage = await this._coverageService.get(this._company, inputData.coverage);
        const dispatch = this._dispatchService.get(this._company, inputData.dispatch);

        return {
            matchingItem,
            coverage,
            dispatch
        };
    }

    private async _getAttributes(inputData: InputAttribute) {
        this._autoAddItems = [];
        const attributesItems: ItemGroupsByAttribute = await this._attributesService.getItems(this._company, inputData);

        let items: ItemBuyItemGroups = await this._condition(attributesItems, inputData);
        await this._exceptionHandling(inputData);

        const result = {
            matchingItem: items,
            autoAddItems: this._autoAddItems
        };

        return result;
    }

    /**
     * 자상/자손 자차에 맞는 가입조건을 컨트롤 한다.
     *
     * 자동 가입조건
     *
     * @param {SubAttributeItems} attributes
     * @private
     */
    private async _condition(itemGroups: ItemGroupsByAttribute, inputData: InputAttribute): Promise<ItemBuyItemGroups> {
        const itemGroupKeys = Object.keys(itemGroups);
        const inputCondition = inputData.condition;

        let result = [];

        for (const itemGroupKey of itemGroupKeys) {
            const combination = itemGroups[itemGroupKey];
            const combinationItems = combination.items;

            let checkCombinationItem = true;

            let hasNo10 = false;
            let hasNo4 = false;
            let hasNo34 = false;
            let hasNo17 = false;
            let hasNo18 = false;
            let hasNo19 = false;
            let hasNo20 = false;
            let hasNo21 = false;
            let hasNo22 = false;
            let hasNo23 = false;
            let hasNo24 = false;
            let hasNo25 = false;

            for (const item of combinationItems) {
                const condition = item.condition;
                checkCombinationItem = this._attributesService.checkExceptionCondition(condition, inputCondition, item);
                if (!checkCombinationItem) {
                    break;
                }
                const title = item.title;
                // 예외처리 자동차상해 - Family 통합보장 특별약관
                if (title === 'WorSHIP 사망 및 후유장애 확대보상 특별약관') {
                    hasNo10 = true;
                } else if (title === '주말 사망 및 후유장애 확대보상 특별약관') {
                    hasNo4 = true;
                } else if (title === '다른 자동차 차량손해담보 특별약관') {
                    hasNo19 = true;
                } else if (title === '다른 자동차 차량손해담보 부모 및 자녀확대 추가 특별약관') {
                    hasNo20 = true;
                } else if (title === '외제차 충돌시 확대보상 특별약관') {
                    hasNo34 = true;
                } else if (title === '차량단독사고 손해보상 특별약관') {
                    hasNo17 = true;
                } else if (title === '자기차량손해 초과수리비 보상 특별약관') {
                    hasNo18 = true;
                } else if (title === '다른 자동차 차량손해담보 특별약관') {
                    hasNo19 = true;
                } else if (title === '다른 자동차 차량손해담보 부모 및 자녀확대 추가 특별약관') {
                    hasNo20 = true;
                } else if (title === '차량 신가보상 특별약관') {
                    hasNo21 = true;
                } else if (title === '자기차량손해 대여자동차지원 특별약관II') {
                    hasNo22 = true;
                } else if (title === '대여자동차 차량손해 담보 특별약관') {
                    hasNo23 = true;
                } else if (title === '레포츠용품 손해담보 특별약관') {
                    hasNo24 = true;
                } else if (title === '자기차량손해 확대보상 특별약관') {
                    hasNo25 = true;
                }
            }

            if (checkCombinationItem) {
                if ((hasNo4 || hasNo10) && inputData.coverage.accident && inputData.coverage.accident.death.value >= 100000000) {
                    checkCombinationItem = false;
                } else if (hasNo20 && !hasNo19) {
                    checkCombinationItem = false;
                } else if (hasNo34 && inputData.coverage.object && inputData.coverage.object.value >= 100000000) {
                    checkCombinationItem = false;
                } else if ((hasNo18 || hasNo19 || hasNo20 || hasNo21 || hasNo22 || hasNo23 || hasNo24 || hasNo25)
                    && !(inputData.condition.selfCar && hasNo17)) {
                    checkCombinationItem = false;
                }
            }

            // 스코어 순서이기 떄문에 빠르게 나온걸 items에 넣고 리턴한다.
            if (checkCombinationItem) {
                if (!combination.autoAddItems) {
                    combination.autoAddItems = [];
                }
                if (hasNo22) {
                    combination.autoAddItems.push(await this._attributesService.getItemInfo(this._company, '대여자동차 차량손해 담보 특별약관'));
                }
                result.push(combination);
            }
        }

        result = result
            .sort((a, b) =>
                (a.score.overCount + a.score.sameCount + a.score.needCount) >
                (b.score.overCount + b.score.sameCount + b.score.needCount) ? 1
                    : (a.score.overCount + a.score.sameCount + a.score.needCount) <
                    (b.score.overCount + b.score.sameCount + b.score.needCount) ? -1 : 0
                    || a.groupSize > b.groupSize ? -1 : a.groupSize < b.groupSize ? 1 : 0);

        return result[0];
    }

    private async _exceptionHandling(inputData: InputAttribute) {
        // 이건 인풋 데이터의 가입조건에 따라 공통 사항
        if (inputData.condition.uninsuredCar) {
            const autoItem = await this._attributesService.getItemInfo(this._company, '다른 자동차 운전담보 특별약관');
            this._autoAddItems.push(autoItem);
        }
    }
}