import { RethinkDB } from '../../module/rethinkdb.module';
import { InputCoverage } from '../../models/coverage.interface';
import { Config } from '../../config/config';

export class CoverageService {
    private _r: any;
    private _coverage: any;

    constructor() {
        this._r = RethinkDB.r;
        this._init();
    }

    async get(company: string, input: InputCoverage) {
        const companyCoverages: any = await this._getCompanyCoverage(company);
        const coverage = this._getCoverage(input, companyCoverages);

        const result = coverage;
        if (input.accident) {
            result.accident = this._getAccidentCoverage(input.accident, companyCoverages);
        }
        if (input.selfCar) {
            result.selfCar = this._getSelfCarCoverage(input.selfCar, companyCoverages.selfCar);
        }
        return result;
    }

    /**
     * 자차, 자손/자상 같은겨우는 예외로 둔다. 물적할증 금액도 필요 없다.
     *
     * @param ourCoverage
     * @param {InputCoverage} input
     * @private
     */
    private _getCoverage(input: InputCoverage, companyCoverages: any) {
        const ourCoverages = this._coverage;
        const inputKeys = Object.keys(input);
        const result: any = {};
        for (const inputKey of inputKeys) {
            if (inputKey !== 'accident' && inputKey !== 'selfCar' && inputKey !== 'surcharge') {
                const companyCoverageData = companyCoverages[inputKey].sort((a: any, b: any) => a.value - b.value);
                const inputValue = input[inputKey].value;
                const ourCoverageData = ourCoverages[inputKey].sort((a: any, b: any) => a.value - b.value);
                const ourCoverageDataSize = ourCoverageData.length;
                // 회사의 마지막 데이터를 가지고 온다.
                const companyCoverageLastData = companyCoverageData[companyCoverageData.length - 1];

                let score = 0;
                let lastDataScore = null;
                let startScore = null;
                for (let i = 0; i < ourCoverageDataSize; i++) {
                    const ourCoverageDataValue = ourCoverageData[i].value;
                    if (ourCoverageDataValue >= inputValue) {
                        const selectData = companyCoverageData
                            .filter((companyCoverage: any) => ourCoverageDataValue === companyCoverage.value);

                        if (selectData.length > 0) {
                            result[inputKey] = {score, ...selectData[0]};
                            break;
                        }
                        score++;

                        if (ourCoverageDataValue === inputValue) {
                            startScore = i;
                        }
                    }

                    if (ourCoverageData[i].value === companyCoverageLastData.value) {
                        lastDataScore = i;
                    }
                }

                if (!result[inputKey]) {
                    const selectData = companyCoverageData[companyCoverageData.length - 1];
                    result[inputKey] = {score: startScore - lastDataScore, ...selectData};
                }
            }
        }

        return result;
    }

    /**
     * 자손/자상 관련 점수 구하기
     *
     * @private
     */
    private _getAccidentCoverage(input: any, companyCoverages: any) {
        const ourCoverages = this._coverage;
        let ourCoverageData: any = null;
        let companyAccidentCoverageData = null;

        if (input.isCar) {
            ourCoverageData = ourCoverages.carAccident;
            companyAccidentCoverageData = companyCoverages.carAccident;
        } else {
            ourCoverageData = ourCoverages.bodyAccident;
            companyAccidentCoverageData = companyCoverages.bodyAccident;
        }
        ourCoverageData = ourCoverageData.sort((a: any, b: any) => a.death.value - b.death.value || a.injury.value - b.injury.value);
        companyAccidentCoverageData =
            companyAccidentCoverageData
                .sort((a: any, b: any) => a.death.value - b.death.value || a.injury.value - b.injury.value);

        const inputDeath = input.death.value;
        const inputInjury = input.injury.value;

        if (inputDeath === 0) {
            return {death: {title: '미가입', value: 0}, injury: {title: '미가입', value: 0}, score: 0};
        }

        let next = null;
        let before = {
            value: 0
        };
        let result: any = {};

        for (const companyCoverage of companyAccidentCoverageData) {
            const death = companyCoverage.death.value;
            const injury = companyCoverage.injury.value;
            if (death === inputDeath && injury === inputInjury) {
                return {score: 0, ...companyCoverage};
            }

            if (death === inputDeath) {
                result.death = companyCoverage.death;
            } else if (!next && inputDeath < death) {
                next = companyCoverage.death;
            } else if (death < inputDeath && death > before.value) {
                before = companyCoverage.death;
            }
        }

        if (!result.death) {
            result.death = next ? next : before;
        }

        next = null;
        before = {
            value: 0
        };
        let selectCoverage = companyAccidentCoverageData
            .filter((companyCoverage: any) => companyCoverage.death.value === result.death.value);
        for (const companyCoverage of selectCoverage) {
            const injury = companyCoverage.injury.value;

            if (injury === inputInjury) {
                result.injury = companyCoverage.injury;
            } else if (!next && inputInjury < injury) {
                next = companyCoverage.injury;
            } else if (injury < inputInjury && injury > before.value) {
                before = companyCoverage.injury;
            }
        }
        if (!result.injury) {
            result.injury = next ? next : before;
        }


        let index = 0;
        let startScore = 0;
        let resultScore = 0;
        let beforeMax = -1;
        for (const ourCoverage of ourCoverageData) {
            const death = ourCoverage.death.value;
            const injury = ourCoverage.injury.value;

            if (injury !== beforeMax) {
                beforeMax = injury;
                index++;
            }
            if (death === inputDeath && injury === inputInjury) {
                startScore = index;
            }
            if (death === result.death.value && injury === result.injury.value) {
                resultScore = index;
            }
        }
        const realScroe = resultScore - startScore;
        result.score = realScroe < 0 ? realScroe * -1 : realScroe;
        result.isCar = input.isCar;
        return result;
    }

    /**
     * 별개로 동작
     *
     * @private
     */
    private _getSelfCarCoverage(input: any, companySelfCar: any[]) {
        const ourSelfCarCoverage = this._coverage.selfCar
            .sort((a: any, b: any) =>
                a.rate.value - b.rate.value || a.max.value - b.max.value || a.min.value - b.min.value);
        companySelfCar = companySelfCar
            .sort((a: any, b: any) =>
                a.rate.value - b.rate.value || a.max.value - b.max.value || a.min.value - b.min.value);

        const inputRate = input.rate.value;
        const inputMax = input.max.value;
        const inputMin = input.min.value;

        if (inputRate === 0) {
            return {rate: {title: '미가입', value: 0}, max: {title: '', value: 0}, min: {title: '', value: 0}, score: 0};
        }
        let next = null;
        let before = {
            value: 0
        };
        let result: any = {};
        for (const companyCoverage of companySelfCar) {
            const rate = companyCoverage.rate.value;
            const max = companyCoverage.max.value;
            const min = companyCoverage.min.value;
            if (rate === inputRate && max === inputMax && min === inputMin) {
                return {score: 0, ...companyCoverage};
            }

            if (rate === inputRate) {
                result.rate = companyCoverage.rate;
            } else if (!next && inputRate < rate) {
                next = companyCoverage.rate;
            } else if (rate < inputRate && rate > before.value) {
                before = companyCoverage.rate;
            }
        }

        if (!result.rate) {
            result.rate = next ? next : before;
        }

        next = null;
        before = {
            value: 0
        };
        let selectCompanySelfCar = companySelfCar.filter((companyCoverage: any) => companyCoverage.rate.value === result.rate.value);
        for (const selectSelfCar of selectCompanySelfCar) {
            const max = selectSelfCar.max.value;

            if (max === inputMax) {
                result.max = selectSelfCar.max;
            } else if (!next && inputRate < max) {
                next = selectSelfCar.max;
            } else if (max < inputRate && max > before.value) {
                before = selectSelfCar.max;
            }
        }
        if (!result.max) {
            result.max = next ? next : before;
        }

        next = null;
        before = {
            value: 0
        };
        selectCompanySelfCar = companySelfCar.filter((companyCoverage: any) => companyCoverage.max.value === result.max.value);
        for (const selectSelfCar of selectCompanySelfCar) {
            const min = selectSelfCar.min.value;

            if (min === inputMin) {
                result.min = selectSelfCar.min;
            } else if (!next && inputRate < min) {
                next = selectSelfCar.min;
            } else if (min < inputRate && min > before.value) {
                before = selectSelfCar.min;
            }
        }

        if (!result.min) {
            result.min = next ? next : before;
        }

        let index = 0;
        let startScore = 0;
        let resultScore = 0;
        let beforeMax = -1;
        for (const ourSelfCarCoverageData of ourSelfCarCoverage) {
            const rate = ourSelfCarCoverageData.rate.value;
            const max = ourSelfCarCoverageData.max.value;

            if (max !== beforeMax) {
                beforeMax = max;
                index++;
            }
            if (rate === inputRate && max === inputMax) {
                startScore = index;
            }
            if (rate === result.rate.value && max === result.max.value) {
                resultScore = index;
            }
        }
        const realScroe = resultScore - startScore;
        result.score = realScroe < 0 ? realScroe * -1 : realScroe;
        return result;
    }

    private async _init() {
        await this._getOurCoverage();
    }

    private async _getCompanyCoverage(company: string) {
        const result = await this._r.table(Config.TABLE_COVERAGE_COMPANY).filter({company});
        return result[0];
    }

    private async _getOurCoverage() {
        const result = await this._r.table(Config.TABLE_COVERAGE);
        this._coverage = result[0];
    }
}