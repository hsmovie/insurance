import { RethinkDB } from '../../module/rethinkdb.module';
import {
    Condition,
    InputAttribute,
    InputCondition,
    Item,
    InputMatching,
    SubAttributes, ItemGroupsByAttribute
} from '../../models/attribute.interface';
import { Config } from '../../config/config';

export class AttributesService {
    private _r: any;
    private _companyAttributes: any[];

    constructor() {
        this._r = RethinkDB.r;
    }

    async getItemInfo(company: string, title: string) {
        const autoItem = await this._r.table(Config.TABLE_ITEMS).filter({company, title});
        return autoItem[0];
    }

    checkExceptionCondition(condition: Condition, inputCondition: InputCondition, item: Item) {
        if (condition) {
            const accident = condition.accident;
            // 자상/자손에 대
            if (accident) {
                if (accident.isAnd) {
                    // 체크
                    if (!(accident.condition.carAccident === inputCondition.carAccident
                            && accident.condition.bodyAccident === inputCondition.bodyAccident)) {
                        return false;
                    }
                } else {
                    if (!(accident.condition.carAccident === inputCondition.carAccident
                            || accident.condition.bodyAccident === inputCondition.bodyAccident)) {
                        return false;
                    }
                }
            }
            // 운전자 범위에 대한 검색
            const member: any = item.condition.member;
            if (member) {
                const memberKeys = Object.keys(member);
                let check = false;
                for (const memberKey of memberKeys) {
                    if (member[memberKey] === inputCondition[memberKey]) {
                        check = true;
                        break;
                    }
                }

                if (!check) {
                    return false;
                }
            }
        }
        return true;
    }

    async getItems(company: string, inputData: InputAttribute): Promise<ItemGroupsByAttribute> {
        // 서브속성의 조합 + 기존 속성
        const attribute = this._getAttributes(inputData.matchingAttribute);
        const items: Item[] = await this._getItems(company, inputData.condition, attribute);
        const itemGroups = this._makeItemGroup(items, attribute);
        return itemGroups;
    }

    /**
     * 가입조건(자손/자상, 자차를 제외한 조건들)에 맞는 상품을 가지고 온다.
     *
     * @param {string} company
     * @param condition
     * @returns {Promise<Item[]>}
     * @private
     */
    private async _getItems(company: string, condition: any, attribute: string[]): Promise<Item[]> {
        const r = this._r;
        let query = r.row('company').eq(company).and(r.row('notMatching').eq(false));
        if (condition) {
            const conditionKeys = Object.keys(condition);
            const field = 'condition';
            for (const key of conditionKeys) {
                // 자상/자손, 운전자 범위는 따로 체크한다.
                if (key !== 'accident' && key !== 'member') {
                    let subQuery = r.row.hasFields({'condition': key}).not();
                    subQuery = subQuery.or(r.row(field)(key).eq(null));
                    subQuery = subQuery.or(r.row(field)(key).eq(condition[key]));
                    query = query.and(subQuery);
                }
            }
        }

        const conditionItems = await r.table(Config.TABLE_ITEMS).filter(query);
        const set = new Set();
        const items: any = [];

        for (const conditionItem of conditionItems) {
            const keys = Object.keys(conditionItem.matchingAttribute);
            for (const key of keys) {
                if (key === 'subAttributeGroups') {
                    for (const subAttributeGroup of conditionItem.matchingAttribute.subAttributeGroups) {
                        if (attribute.includes(subAttributeGroup)) {
                            set.add(subAttributeGroup);
                            if (!items.includes(conditionItem)) {
                                items.push(conditionItem);
                            }
                        }
                    }
                } else if (attribute.includes(key)) {
                    set.add(key);
                    if (!items.includes(conditionItem)) {
                        items.push(conditionItem);
                    }
                }
            }
        }

        this._companyAttributes = Array.from(set);

        return items;
    }

    private _getAttributes(matchingAttribute: InputMatching) {
        const subAttributeGroup = this._makeAttributeListBySubAttribute(matchingAttribute.subAttributes);
        const attributes = Object.keys(matchingAttribute)
            .filter(attribute => attribute !== 'subAttributes' && matchingAttribute[attribute]);
        return [...subAttributeGroup, ...attributes];
    }

    private _makeAttributeListBySubAttribute(subAttribute: SubAttributes) {
        const whos: string[] = [...subAttribute.who, 'fam.all'];
        const whens1: string[] = subAttribute.when1;
        const whens2: string[] = subAttribute.when2;
        const whats: string[] = subAttribute.what;
        const attributeList = [];

        for (const who of whos) {
            for (const when1 of whens1) {
                for (const when2 of whens2) {
                    for (const what of whats) {
                        attributeList.push(`${who}-${when1}-${when2}-${what}`);
                    }
                }
            }
        }
        return attributeList;
    }

    private _makeItemGroup(items: Item[], selectAttribute: string[]): ItemGroupsByAttribute {
        const itemsSize = items.length;

        const itemGroup: ItemGroupsByAttribute = {};

        const letLen = Math.pow(2, itemsSize);

        for (let i = 0; i < letLen; i++) {
            let detailItems: Item[] = [];
            let key = '';

            let attributes: string[] = [];
            for (let j = 0; j < itemsSize; j++) {
                if (i & Math.pow(2, j)) {
                    const item = items[j];
                    if (key) {
                        key = `${key}^^${item.title}`;
                    } else {
                        key = `${item.title}`;
                    }
                    detailItems.push(item);
                    const attributeObjects = {...item.matchingAttribute};
                    delete attributeObjects.subAttributeGroups;
                    const detailAttributes: string[] = Object.keys(attributeObjects);

                    if (item.matchingAttribute.subAttributeGroups) {
                        attributes = [...attributes, ...item.matchingAttribute.subAttributeGroups];
                    }
                    for (const detailAttribute of detailAttributes) {
                        attributes.push(detailAttribute);
                    }
                }
            }

            const companyAttributes = this._companyAttributes;
            const checkAttributeAndGet = companyAttributes.filter(attribute => attributes.includes(attribute));

            if (detailItems.length > 0 && checkAttributeAndGet.length > 0) {
                let overCount = 0;
                let sameCount = 0;

                let needCount = selectAttribute.length - checkAttributeAndGet.length;
                const sameAttributes: any = [];

                for (const attribute of attributes) {
                    if (companyAttributes.includes(attribute)) {
                        if (sameAttributes.includes(attribute)) {
                            sameCount++;
                        } else {
                            sameAttributes.push(attribute);
                        }
                    } else {
                        overCount++;
                    }
                }

                const score = {overCount, sameCount, needCount};
                const setAttributes = new Set(attributes);
                itemGroup[key] = {
                    groupSize: detailItems.length,
                    items: detailItems,
                    attributes: Array.from(setAttributes),
                    score,
                    selectAttribute
                };
            }
        }

        return itemGroup;
    }
}
