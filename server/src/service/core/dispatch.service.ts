import { Dispatch } from '../../models/dispatch.interface';

export class DispatchService {

    constructor() {
    }

    get(company: string, userData: Dispatch) {
        const companies = ['삼성화재', '더케이손해보험', 'MG손해보험', '메리츠화재보험', '롯데손해보험', 'KB손해보험', '현대해상화재보험', '흥국화재', '동부화재', 'AXA손해보험', '한화손해보험'];
        const selectedDispatch: any = {};
        companies.map(company => {
            selectedDispatch[company] = [];
        });

        if (userData && userData.dispatch.value !== 0) {
            /*
                title: 특약이름
                division: 반드시 1개만 선택되어야 하는 특약은 true, 추가특약은 false (상품명으로 구분하는것이 쉽지 않아 넣었음. 추후 수정)

                현제 기획, 디자인 산출물 상 긴급출동이 어떻게 노출될지 확실하지 않아서 필드를 title, division 두개로만 하였습니다.
                그리고 택 1 특약과 추가특약
            */
            // 가입 시 필수로 택 1 해야하는 특약
            selectedDispatch['삼성화재'].push({title: '애니카서비스', division: true});
            selectedDispatch['메리츠화재보험'].push({title: 'Readycar 긴급출동서비스 특별약관', division: true});
            if (userData.extraService) {
                selectedDispatch['MG손해보험'].push({title: '조이카 긴급출동서비스 특별약관', division: true});
                selectedDispatch['흥국화재'].push({title: '마이카플러스서비스 담보 특별약관', division: true});

                selectedDispatch['롯데손해보험'].push({title: '해피카서비스2 추가 특별약관', division: false});
                selectedDispatch['삼성화재'].push({title: '단 Door to Door 서비스 추가특별약관', division: false});
                selectedDispatch['현대해상화재보험'].push({title: 'Road 차량진단 서비스 추가특별약관', division: false});
            } else {
                selectedDispatch['MG손해보험'].push({title: '조이카 프리미엄 서비스 특별약관', division: true});
                selectedDispatch['흥국화재'].push({title: '마이카서비스 담보 특별약관', division: true});
            }

            if (userData.dispatch.value === 10) {
                selectedDispatch['더케이손해보험'].push({title: '긴급출동 서비스 특별약관', division: true});
                selectedDispatch['AXA손해보험'].push({title: '긴급출동서비스 특별약관', division: true});
                selectedDispatch['롯데손해보험'].push({title: '해피카서비스 특별약관(실속형)', division: true});
                if (userData.extraService) {
                    selectedDispatch['KB손해보험'].push({title: '뉴매직카서비스A 특별약관', division: true});
                    selectedDispatch['한화손해보험'].push({title: '한화 긴급출동 플러스 서비스 특별약관', division: true});
                } else {
                    selectedDispatch['한화손해보험'].push({title: '한화 긴급출동 서비스 특별약관', division: true});
                    selectedDispatch['KB손해보험'].push({title: '매직카서비스 특별약관', division: true});
                }
            } else if (userData.dispatch.value === 20 || userData.dispatch.value === 30) {
                selectedDispatch['한화손해보험'].push({title: '한화 긴급출동 플러스 서비스 특별약관', division: true});
                selectedDispatch['롯데손해보험'].push({title: '해피카서비스 특별약관(기본형)', division: true});

                selectedDispatch['더케이손해보험'].push({title: '긴급출동 서비스2 특별약관', division: true});
                selectedDispatch['KB손해보험'].push({title: '뉴매직카서비스A(50km) 특별약관', division: true});
                selectedDispatch['AXA손해보험'].push({title: '긴급출동서비스 확장 특별약관', division: true});
            } else {
                selectedDispatch['한화손해보험'].push({title: '한화 긴급출동 서비스 특별약관', division: true});
                selectedDispatch['더케이손해보험'].push({title: '긴급출동 서비스2 특별약관', division: true});
                selectedDispatch['AXA손해보험'].push({title: '긴급출동서비스 확장 특별약관', division: true});
                selectedDispatch['롯데손해보험'].push({title: '해피카서비스 특별약관(안심형)', division: true});
                selectedDispatch['KB손해보험'].push({title: '뉴매직카서비스A(50km) 특별약관', division: true});
            }

            // 전기차 필터
            if (!userData.electricCar) {
                selectedDispatch['현대해상화재보험'].push({title: '하이카(Hicar)서비스 특별약관', division: true});
                if (userData.dispatch.value > 10 || userData.extraService) {
                    selectedDispatch['동부화재'].push({title: '프로미카 오토케어서비스 특별약관', division: true});
                } else {
                    selectedDispatch['동부화재'].push({title: '프로미카 SOS서비스 특별약관', division: true});
                }
            } else {
                selectedDispatch['동부화재'].push({title: '전기자동차 SOS 서비스 특별약관', division: true});
                selectedDispatch['현대해상화재보험'].push({title: '하이카(Hicar)서비스 특별약관(전기자동차)', division: true});
            }

            // 추가로 들어갈 특약들
            if (userData.dispatch.value > 10) {
                selectedDispatch['삼성화재'].push({title: '긴급견인서비스 확대 추가특별약관', division: false});
                selectedDispatch['현대해상화재보험'].push({title: '견인 서비스 확장 추가특별약관', division: false});
                if (userData.extraService) {
                    selectedDispatch['흥국화재'].push({title: '마이카플러스서비스 확대담보 추가특별약관', division: false});
                } else {
                    selectedDispatch['흥국화재'].push({title: '마이카서비스 확대담보 추가특별약관', division: false});
                }
            }
            if (userData.dispatch.value === 20) {
                selectedDispatch['메리츠화재보험'].push({title: '긴급견인서비스 확대 추가특별약관(1)', division: false});
            } else if (userData.dispatch.value === 30 || userData.dispatch.value === 40) {
                selectedDispatch['메리츠화재보험'].push({title: '긴급견인서비스 확대 추가특별약관(2)', division: false});
            } else if (userData.dispatch.value >= 50) {
                selectedDispatch['메리츠화재보험'].push({title: '긴급견인서비스 확대 추가특별약관(3)', division: false});
            }

            if (userData.dispatch.value >= 40) {
                selectedDispatch['한화손해보험'].push({title: '긴급 견인서비스 확대 추가 특별약관', division: false});
            }

            if (userData.unlockService) {
                selectedDispatch['동부화재'].push({title: '치 해제 서비스 제외 추가특별약관', division: false});
                selectedDispatch['메리츠화재보험'].push({title: '잠금장치해제서비스제외 추가특별약관', division: false});
            }
        }
        return selectedDispatch[company];
    }
}
