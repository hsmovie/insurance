import * as chai from 'chai';
import chaiHttp = require('chai-http');

chai.use(chaiHttp);

const http = chai.request('http://localhost:3000/api/item');
const expect = chai.expect;

let id: any = null;
let description: any = '';

describe('item', () => {
    it('상품 정보를 가지고 온다.', done => {
        http
            .get('/')
            .query({company: '동부화재'})
            .then(res => {
                expect(res.status).to.be.equal(200);
                id = res.body[0].id;
                description = res.body[0].description;
                done();
            });
    });
    it('상품 정보를 저장한다.', done => {
        http
            .post('/')
            .send({id, description: 'test2'})
            .then(res => {
                http
                    .get('/test')
                    .query({id})
                    .then(res => {
                        expect(res.status).to.be.equal(200);
                        expect(res.body.description).to.be.equal('test2');
                        done();
                    });
            });
    });
    after(done => {
        http
            .post('/')
            .send({id, description: description})
            .then(res => {
                http
                    .get('/test')
                    .query({id})
                    .then(res => {
                        expect(res.status).to.be.equal(200);
                        expect(res.body.description).to.be.equal(description);
                        done();
                    });
            });
    });
});