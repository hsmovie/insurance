import * as chai from 'chai';
import chaiHttp = require('chai-http');
import { validation } from '../../../src/module/validation.module';

chai.use(chaiHttp);

const http = chai.request('http://localhost:3000/api/dashboard');
const expect = chai.expect;

describe('dashboard Test', () => {

    it('validation', done => {
        const userInputData: any = {
            'coverage': {
                'person2': {'title': '무한', 'value': 9999999999},
                'object': {'title': '2천만원', 'value': 20000000},
                'accident': {
                    'isCar': false,
                    'death': {'title': '1천5백만원', 'value': 15000000},
                    'injury': {'title': '1천5백만원', 'value': 15000000}
                },
                'uninsuredCar': {'title': '미가입', 'value': 0},
                'surcharge': {'title': '200만원', 'value': 2000000},
                'selfCar': {
                    'rate': {'title': '20%', 'value': 20},
                    'min': {'title': '20만원', 'value': 200000},
                    'max': {'value': 2000000, 'title': '200만원'}
                }
            },
            'matchingAttribute': {
                'subAttributes': {
                    'who': ['coup.fem', 'fam.50'],
                    'when1': ['mycar', 'public', 'overseas', 'hitrun', 'church', 'othercar', 'walk'],
                    'when2': ['anytime', 'commute', 'wkends', 'golf', 'seatbelt', 'schzone'],
                    'what': ['injury', 'dead', 'burn', 'abort', 'disable', 'dental', 'hitreat', 'bloodless', 'plastic', 'brain', 'fracture', 'growth', 'hiroom', 'hospitalize']
                },
                'legalSupport': true,
                'foreignCarAccident': true,
                'driveOtherCar': true,
                'driverRangeOtherCar': true,
                'damageOtherCar': true,
                'substitutiveDriver': true,
                'damageByOtherCar': true,
                'expandRangeSelfCar': true,
                'limitRangeSelfCar': false,
                'limitUninsuredCar': false,
                'sevenYearsUpperForeignCar': true,
                'transportationCost': true,
                'rentCarCost': false,
                'leisureGoods': true,
                'newCarCost': true,
                'devaluateCarCost': true,
                'imprintSupport': true,
                'transportCarSupport': true
            },
            'condition': {
                'person2': true,
                'bodyAccident': true,
                'carAccident': false,
                'uninsuredCar': true,
                'selfCar': true,
                'me': true,
                'couple': null,
                'children': null,
                'parents': null,
                'anyone': null,
                'designatePerson': null,
                'domesticCar': true,
                'foreignCar': false,
                'electricCar': false,
                'sixMonthsBelow': true,
                'twelveMonthsBelow': false,
                'threeYearsBelow': false,
                'fiveYearsBelow': false,
                'sevenYearsBelow': false,
                'sevenYearsUpper': false
            },
            'dispatch': {
                'dispatch': {'title': '10km', 'value': 10},
                'extraService': true,
                'unlockService': true,
                'electricCar': false
            },
            'gender': 'male',
            'birthday': '1985-08-14',
            'accidentMode': 'body',
            'selectedPackage': null,
            'selectedCompany': {'name': '현대 자동차', 'value': 'hyundai'},
            'selectedDriver': {'name': '본인', 'img': 'me', 'value': 'me'},
            'selectedDate': {'title': '6개월 이하', 'value': 'sixMonthsBelow'},
            'mode': 'design'
        };
        http
            .post('/')
            .send(userInputData)
            .then(res => {
                expect(res.status).to.be.equal(200);
                expect(res.body).to.be.an('object');
                done();
            });
    });
});
