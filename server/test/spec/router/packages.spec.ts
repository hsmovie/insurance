import * as chai from 'chai';
import chaiHttp = require('chai-http');

chai.use(chaiHttp);

const http = chai.request('http://localhost:3000/api/packages');
const expect = chai.expect;

describe('packages Test', () => {
    it('base', done => {
        http
            .get('/')
            .then(res => {
                expect(res.status).to.be.equal(200);
                expect(res.body).to.be.an('array');
                done();
            });
    });
});
