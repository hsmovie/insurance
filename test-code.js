// const numbers = [];
//
// for (var i = 0; i < 10; i++) {
//     numbers.push(i);
// }
//
// sumEven(numbers);
//
// const sumEven = (array) => {
//     const total = 0;
//
//     for (let i in array) {
//         if (array[i] % 2 === 0) {
//             let lastNumber = array[i];
//             total += array[i];
//         }
//     }
//
//     console.log(`total => ${total}`);
//     console.log(`last number => ${lastNumber}`);
// };
//
//
// // main으로 함수를 변경 시켜준 이유는 처음 시작을 여기서 한다는 가독성 때문이다.
// const main = () => {
//     const numbers = [];
//     // var는 es5문법으로 가급적 es2015문법으로 변경처리 한다.
//     for (let i = 0; i < 10; i++) {
//         numbers.push(i);
//     }
//
//     sumEven(numbers);
// };
//
// const sumEven = (array) => {
//     // const는 상수이기 때문에 let으로 변경 시켜준다.
//     let total = 0;
//     // let은 var와 다르게 블록 스코프 이기 때문에 위의 문제에서 if문 안에 있던것을 밖으로 이동
//     let lastNumber = 0;
//     // array.length를 밖으로 쓰는건 스코프체인의 활성화 객체를 찾아가는 탐색 경로를 거치게 되므로 응답속도 저하가 된다. 그래서 밖으로 빼는것이 좋다.
//     const arrayLength = array.length;
//     // for in은 성능적인 이슈가 있어서 변경 처리 해야된다.
//     // for~in은 배열은 단순 객체로 취급하기 때문에 순서의 보장이 없으며, 열거 가능한 모든 것을 반복하고 다음 순서를 찾아야되는 비용이 발생된다.
//     for (let i = 0; i < arrayLength; i++) {
//         if (array[i] % 2 === 0) {
//             lastNumber = array[i];
//             total += array[i];
//         }
//     }
//
//     console.log(`total => ${total}`);
//     console.log(`last number => ${lastNumber}`);
// };
//
// main();